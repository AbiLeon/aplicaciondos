package com.example.aplicaciondos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var editTextEmail:TextInputEditText
    private lateinit var editTextPassword:TextInputEditText
    private lateinit var button: MaterialButton

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_login_login->validateLogin()
        }
    }

    fun validateLogin(){
        if(editTextEmail.editableText.isEmpty()){
            editTextEmail.error = getString(R.string.error_text)
        }else if(editTextPassword.editableText.isEmpty()){
            editTextPassword.error = getString(R.string.error_text)
        } else {
            editTextEmail.error = null
            editTextPassword.error = null
        }

        val isValid = editTextEmail.editableText.isNotEmpty() && editTextPassword.editableText.isNotEmpty()

        if(isValid){
            Toast.makeText(this,R.string.wellcome_message, Toast.LENGTH_SHORT).show()
        }



    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextEmail = findViewById(R.id.tie_email)
        editTextPassword = findViewById(R.id.tie_pwd)
        button = findViewById(R.id.btn_login_login)

        button.setOnClickListener(this)
    }
}
